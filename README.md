# chapter_reader IDEA 在线&本地阅读插件

# 介绍
"chapter_reader" 是一款IDEA 在线 & 本地 小说阅读插件
为了帮助各位道友在资本主义的魔爪下，有一个可以调节情绪且可以畅游知识海洋的环境。

## 因暂时无法继续工作，没有资本持续投资本插件。处理问题及迭代周期增大，如果各位道友觉得好用不忘点点赞助。

✅ 历史记录自动保存 

✅ 自定义适配想看的网站

✅ 在线章节阅读 - gitee配置文件中已配置的网站

✅ 本地txt章节阅读 

✅ 本地epub章节阅读

✅ 本地txt简易阅读 

✅ 本地txt换行符阅读

遇到问题及时加QQ群，问各位道友或私聊wind。
- 插件：chapter_reader
- 作者：wind
- 邮件：wind_develop@163.com
- QQ群：1群：252192636，2群：251897630（有疑问会有各位道友或作者在线教学）
- gitee：https://gitee.com/wind_invade/chapter_reader

# 安装方式
1. IDEA插件市场直接安装：File | Settings | Plugins | 搜索chapter_reader
2. 通过idea插件网下载安装：https://plugins.jetbrains.com/plugin/16544-chapter-reader

# 版本号说明（idea插件市场搜索时，会自动显示匹配当前版本）：
1.【x.x.x 如：2.1.5】版本（低版本IDEA）
![输入图片说明](images/dbb.png)

2.【x.x.x-high 如：2.1.5-high】版本（高版本IDEA）
![输入图片说明](images/gbb.png)


# 【初次使用必看】使用说明
远程阅读操作方式
![输入图片说明](images/远程阅读操作方式.mp4)
本地txt正则阅读操作方式
![输入图片说明](images/本地阅读模式演示.mp4)
[详细使用说明文档](https://gitee.com/wind_invade/chapter_reader/blob/master/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.MD)

# 【网站适配必看】在线阅读配置说明
[在线阅读配置说明](https://gitee.com/wind_invade/chapter_reader/blob/master/%E5%9C%A8%E7%BA%BF%E9%98%85%E8%AF%BB%E7%BD%91%E7%AB%99%E9%80%82%E9%85%8D.MD)

# 菜单变灰、插件未启用问题
在【插件设置】中，启用插件即可

# 快捷键设置方法
![输入图片说明](images/kjj.png)
按照路径依次打开目录 IDEA | File | Settings | Keymap | Plug-ins | chapter_reader 不要搜索

翻页推荐快捷键，其它自行设置：
- 小键盘"+" "-"(目前最轻松的体验方式)
- 鼠标多功能按键
- 按键 + 鼠标滚轮 (必须是按键+鼠标滚轮, 否则会因优先级的问题, 无法生效)
- 组合快捷键(会很累)

# 如出现右下角有弹窗提示，操作步骤
![输入图片说明](images/tcyc.png)
File | Settings | Appearance & Behavior | Notifications | 搜索novel  | 将Popup 修改为 No popup

# 截图演示

## 菜单展示
![输入图片说明](images/cdzs.png)

## 设置菜单
![输入图片说明](images/szcd.png)

## 左下角展示（需将鼠标焦点触发在IDEA内部窗口中）
### * 如果左下角不展示内容
1. 新版本: 先点击右上角Notifications，再看左下角是否显示，然后关闭Notifications窗口就可以正常显示了（2022.1 / 2023.1版本尝试后均可正常显示）

2. 旧版本：不知道有没有这种情况，也可以使用相同的办法。点击Event Log

3. 新版本左下角默认显示的类导航，右键底部的标签栏，第二个 Navigation Bar 选择 Top 即可跟旧版本一样

4. 或者在 插件设置 中使用 导航栏阅读 模式
![输入图片说明](images/zxjzs.png)


## 弹出框展示（需将鼠标焦点放在文件内）
![输入图片说明](images/tckzs.png)

## 导航栏展示
![输入图片说明](images/dhlzs.png)

## 全章节阅读（右侧窗口，整章阅读，隐蔽性低）
双击鼠标左键隐藏上方按钮

双击鼠标左键并保持不松，拖动一下鼠标即可显示
![输入图片说明](images/qzjzs.png)

可以略微隐蔽些
![输入图片说明](images/qzjyd.jpg)

# 5.特此声明（侵权免责声明）

```
无任何商业用途，无任何侵权想法。但如发现侵权或其它问题请 及时 且 成功 与作者本人取得联系。
作者本人会在第一时间进行相关内容的删除。

插件：chapter_reader
作者：wind
邮件：wind_develop@163.com
QQ群：252192636（有疑问会有各位道友或作者在线教学）
博客园：https://www.cnblogs.com/winddniw/p/15689784.html
csdn： https://blog.csdn.net/com_study/article/details/115709454（吃相难看，会渐渐放弃）

在这祝大家工作开心^_^
```

# 赞助一下~ 鼓励持续更新~
![输入图片说明](images/zc.png)

